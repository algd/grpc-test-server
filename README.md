# GRPC client-server example

In order to run the javascript GRPC client:
```sh
$ git clone https://gitlab.com/algd/grpc-test-server.git
$ cd grpc-test-server/src/main/node/
$ npm start
```
This will download the required dependencies (grpc) and run the main.js that will connect to the AWS load balancer sending random requests (it is already configured for it).
There is no problem if the way keys/values are generated is modified as part of the test.

In order to run the grpc scala server:
```sh
$ sbt run
```
or, to create the jar:
```sh
$ sbt assembly
```
(GRPC 0.9.1 version is being used)
