package io.github.algd.grpc

import com.redis.RedisClientPool
import com.typesafe.config.ConfigFactory
import io.grpc.netty.NettyServerBuilder

/**
 * Created by agarcia on 8/02/16.
 */
object GrpcServer extends App {
  //Config
  private val config = ConfigFactory.load()
  val (redisHost, redisPort) = (config.getString("redis.host"), config.getInt("redis.port"))
  val serverPort = config.getInt("server.port")

  // Redis pool
  val pool = new RedisClientPool(redisHost, redisPort)

  // Server
  val server = NettyServerBuilder.forPort(serverPort)
    .addService(UserServiceGrpc.bindService(new UserServiceImpl(pool)))
    .build().start()

  println("Server listening in port: " + serverPort)
  // Prevent application from finishing
  while(true) {
    Thread.sleep(10000)
  }
}