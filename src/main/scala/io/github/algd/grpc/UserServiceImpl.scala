package io.github.algd.grpc

import com.redis._
import io.github.algd.grpc.UserServiceOuterClass.{StoreUserReply, StoreUserRequest, GetUserReply, GetUserRequest}
import io.grpc.stub.StreamObserver

/**
 * Created by agarcia on 8/02/16.
 */
class UserServiceImpl(pool: RedisClientPool) extends UserServiceGrpc.UserService {

  override def storeUser(r: StoreUserRequest, responseObserver: StreamObserver[StoreUserReply]): Unit = {
    pool.withClient { client =>
      client.set(r.getId, r.getName)
    }

    println(s"Stored user with id (${r.getId}) as (${r.getName})")

    val reply = StoreUserReply.newBuilder().setId(r.getId).build()
    responseObserver.onNext(reply)
    responseObserver.onCompleted()
  }

  override def getUser(r: GetUserRequest, responseObserver: StreamObserver[GetUserReply]): Unit = {
    print(s"User requested with id (${r.getId}) ")
    val builder = GetUserReply.newBuilder()

    val reply = (pool.withClient { client =>
      client.get(r.getId)
    } match {
      case Some(name) =>
        println(s"exists with name ($name)")
        builder.setName(name)
      case None =>
        println("doesn't exist")
        builder.setName("Not found")
    }).setId(r.getId).build()

    responseObserver.onNext(reply)
    responseObserver.onCompleted()
  }
}
