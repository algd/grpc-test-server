var grpc = require('grpc');
var userService = grpc.load('../protobuf/user_service.proto').io.github.algd.grpc;

function request(client) {
  var id = Math.floor((Math.random() * 15) + 1).toString();

  if (Math.random()<.5) {
    // Store request
    var name = 'name' + Math.floor((Math.random() * 1000) + 1).toString();
    client.storeUser({id: id, name: name}, function(err, response) {
      console.log('Stored user ' + name + ' with id: ' + response.id);
    });
  } else {
    // Get request
    client.getUser({id: id}, function(err, response) {
        console.log('Get user with id '+ id + ' : ' + response.name);
    });
  }
}

function main() {
  var host = 'grpcbalancer-1837153269.eu-west-1.elb.amazonaws.com'

  var client = new userService.UserService(host + ':50051',
                                       grpc.credentials.createInsecure());

  // Make a request every second
  setInterval(request, 1000, client);
}

main();