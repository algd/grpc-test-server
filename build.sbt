import sbtprotobuf.{ ProtobufPlugin => PB }

Seq(PB.protobufSettings: _*)

name := "test-grpc-server"

organization  := "io.github.algd"

version       := "0.0.1-SNAPSHOT"

scalaVersion  := "2.11.7"

scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8")

// Grpc java plugin path

val grpcJavaProtobufPlugin = Path.userHome.absolutePath+"/grpc-java/compiler/build/binaries/java_pluginExecutable/protoc-gen-grpc-java"

val grpcVersion = "0.9.1"
val redisClientVersion = "3.0"

version in PB.protobufConfig := "3.0.0-beta-1"

libraryDependencies ++= Seq(
  "io.grpc" % "grpc-all" % grpcVersion exclude("io.netty", "netty-codec-http"),
  "net.debasishg" %% "redisclient" % redisClientVersion
)

protoc in PB.protobufConfig := "/usr/local/bin/protoc"

protocOptions in PB.protobufConfig ++= Seq(
  "--plugin=protoc-gen-java_rpc="+grpcJavaProtobufPlugin,
  "--java_rpc_out=target/scala-2.11/src_managed/main/compiled_protobuf"
)

assemblyMergeStrategy in assembly <<= (assemblyMergeStrategy in assembly) { (old) =>
  {
    case PathList(ps @ _*) if ps.last endsWith ".properties" => MergeStrategy.first
    case x => old(x)
  }
}